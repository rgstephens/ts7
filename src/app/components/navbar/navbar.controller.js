'use strict';

angular.module('ts7')
  .controller('NavbarCtrl', ['$scope', 'auth', '$modal', '$rootScope', '$location', '$window', 'usergridFactory', 'LocalStore', function ($scope, auth, $modal, $rootScope, $location, $window, usergridFactory, LocalStore) {
    var cacheRoleList;
    $scope.date = new Date();

    $scope.loggedIn = function () {
      var isAuthenticated = usergridFactory.isAuthenticated();
      //var isLoggedIn = auth.loggedIn();
      //console.log('isAuthenticated returns: ' + isAuthenticated + ', auth.loggedIn: ' + auth.loggedIn());
      if (!isAuthenticated) {
        LocalStore.deleteUser();
      }
      return isAuthenticated;
    };

    $scope.logout = function () {
      //console.log('NavbarCtrl.logout');
      usergridFactory.logout();
      $location.path('/login');
    };

    $scope.hasRole = function (role) {
      var itemFound = false;
      role = role.trim();
      if (!cacheRoleList) {
        cacheRoleList = $window.localStorage.getItem('roles');
        if (!cacheRoleList) {
          return false
        } else {
          cacheRoleList = $window.localStorage.getItem('roles').split(',');
        }
      }
      cacheRoleList.forEach(function (item) {
        if ((typeof item) === 'string') {
          if (item.trim() === role) {
            //console.log(item + ": true");
            itemFound = true;
          }
        }
      });
      return itemFound;
    };

    $scope.animationsEnabled = true;

    $scope.openProfileModal = function (size) {
      $scope.currentServer = LocalStore.getCurrentServer();
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        scope: $scope,
        templateUrl: 'app/components/auth/profileModal.html',
        controller: 'ProfileModalCtrl',
        size: size,
        resolve: {
          description: function () {
            return $scope.description;
          }
        }
      });
    };

    $scope.openServerModal = function (size) {
      $scope.currentServer = LocalStore.getCurrentServer();
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        scope: $scope,
        templateUrl: 'app/components/auth/serverModal.html',
        controller: 'ServerModalCtrl',
        size: size,
        resolve: {
          description: function () {
            return $scope.description;
          }
        }
      });
    };

  }])
  .controller('ServerModalCtrl', function ($scope, $modalInstance, LocalStore) {

    $scope.okServer = function () {
      LocalStore.storeCurrentServer($scope.currentServer);
      $modalInstance.close();
    };

    $scope.defaultsServer = function () {
      LocalStore.setServerDefaults();
      $modalInstance.dismiss('cancel');
    };

    $scope.cancelServer = function () {
      $modalInstance.dismiss('cancel');
    };
  })
  .controller('ProfileModalCtrl', function ($scope, $modalInstance, LocalStore, auth) {
    $scope.user = LocalStore.getUser();
    $scope.user.roles = LocalStore.getString('roles');
    $scope.user.password = auth.getPassword();

    $scope.okProfile = function () {
      console.log('ProfileModalCtrl.ok');
//      auth.storeCurrentServer($scope.currentServer);
      $modalInstance.close();
    };

    $scope.cancelProfile = function () {
      $modalInstance.dismiss('cancel');
    };
  })
;
