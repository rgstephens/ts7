'use strict';

angular.module('ts7')
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('users', {
        abstract: true,
        url: '/users',
        templateUrl: 'app/components/users/users.html',
        controller: ['$scope', '$filter', '$window', '$modal', 'usergridFactory', 'ngTableParams', function ($scope, $filter, $window, $modal, usergridFactory, ngTableParams) {
          $scope.tableParams = new ngTableParams({
            page: 1,
            count: 10,
            sorting: {username: 'asc'}
          }, {
            getData: function ($defer, params) {
              //var users = [];
              usergridFactory.listUsers()
                .then(function (users) {
                  //$defer.resolve(users);
                  params.total(users.length);
                  // append the user Role to each user
                  users.forEach(function (user, i) {
                    usergridFactory.getUserRoles(user.username)
                      .then(function (roleList) {
                        user.roles = roleList;
                        if (i >= (users.length - 1)) {
                          users = params.filter() ? $filter('filter')(users, params.filter()) : users;
                          users = params.sorting() ? $filter('orderBy')(users, params.orderBy()) : users;
                          $defer.resolve(users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        }
                      })
                  });
                });
            }
          });

          $scope.hasRole = function (role) {
            var itemFound = false;
            role = role.trim();
            var cacheRoleList = $window.localStorage.getItem('roles').split(',');
            cacheRoleList.forEach(function (item) {
              if ((typeof item) === 'string') {
                if (item.trim() === role) {
                  //console.log(item + ": true");
                  itemFound = true;
                }
              }
            });
            return itemFound;
          };

          $scope.changeUserPassword = function (username) {
            console.log('change password for user: ' + username);
            usergridFactory.getUser(username)
              .then(function (user) {
                user[0].password = 'xyzzy';
                usergridFactory.updateUser(user[0])
                  .then(function (user) {
                    usergridFactory.logEvent('normal', 'user', 'password', 'User password updated: ' + user.username);
                  },
                  function (errorMsg) {
                    console.log('updateUser failed');
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: errorMsg});
                  }
                );
              });
          };

          $scope.resetUserPassword = function (username) {
            console.log('reset password for user: ' + username);
            usergridFactory.resetPassword(username)
              .then(function (user) {
                usergridFactory.logEvent('normal', 'user', 'resetpw', 'User password reset called: ' + username);
                for (var i = $scope.tableParams.data.length - 1; i >= 0; i--) {
                  if ($scope.tableParams.data[i].username == username) {
                    $scope.tableParams.data.splice(i, 1);
                  }
                }
                $scope.tableParams.reload();
              });
          };

          $scope.deleteUser = function (username) {
            console.log('delete user: ' + username + ', reloading table');
            usergridFactory.deleteUser(username)
              .then(function (user) {
                usergridFactory.logEvent('normal', 'user', 'delete', 'User deleted: ' + username);
                for (var i = $scope.tableParams.data.length - 1; i >= 0; i--) {
                  if ($scope.tableParams.data[i].username == username) {
                    $scope.tableParams.data.splice(i, 1);
                  }
                }
                $scope.tableParams.reload();
              });
          };
        }]
      })
      //////////////////
      // Users > View //
      //////////////////
      .state('users.view', {
        url: '/view/:username',
        templateUrl: 'app/components/users/users.edit.html',
        controller: ['$scope', '$state', '$stateParams', '$filter', 'usergridFactory', function ($scope, $state, $stateParams, $filter, usergridFactory) {
          $scope.$state = $state;
          usergridFactory.getUser($stateParams.username)
            .then(function (user) {
              $scope.user = user[0];
              $scope.gender = {};
              $scope.genders = [{name: 'M'}, {name: 'F'}];
              $scope.gender.selected = {name: user[0].gender};
            });

          $scope.okUser = function () {
            $state.go('users.list');
          };
        }]
      })
      //////////////////
      // Users > List //
      //////////////////
      .state('users.list', {
        url: '',
        templateUrl: 'app/components/users/users.list.html'
      })
      ////////////////////
      // Password Modal //
      ////////////////////
      // users/list/password/{{ user.username }}
      .state("users.password", {
        url: "/password/:username",
        onEnter: ['$stateParams', '$state', '$modal', 'usergridFactory', function($stateParams, $state, $modal, usergridFactory) {
          $modal.open({
            templateUrl: 'app/components/users/passwordModal.html',
/*
            resolve: {
              item: function() { new Item(123).get(); }
            },
*/
            controller: ['$scope', function ($scope) {
              $scope.cancel = function () {
                $scope.$dismiss();
              };

              $scope.ok = function () {
                usergridFactory.getAppCreds()
                  .then(function (creds) {
                    usergridFactory.setUserPassword($stateParams.username, '', $scope.user.password)
                      .then(function (user) {
                        usergridFactory.logEvent('normal', 'user', 'password', 'User password updated: ' + $stateParams.username);
                      },
                      function (errorMsg) {
                        console.log('updateUser failed');
                        $scope.alerts = [];
                        $scope.alerts.push({type: 'danger', msg: errorMsg});
                      }
                    );
                  },
                  function (errorMsg) {
                    console.log('failed getting application creds');
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: errorMsg});
                  }
                );
                $scope.$close(true);
              };
            }]
          }).result.finally(function() {
//              $state.go('^');
              $state.go('users.list');
            });
        }]
      })
      ////////////////////
      // Users > Create //
      ////////////////////
      .state('users.create', {
        url: '/create',
        templateUrl: 'app/components/users/users.edit.html',
        controller: ['$scope', '$state', '$filter', 'usergridFactory', function ($scope, $state, $filter, usergridFactory) {
          $scope.$state = $state;
          $scope.alerts = [];
          $scope.gender = {};
          $scope.gender.selected = {name: 'M'};
          $scope.genders = [{name: 'M'}, {name: 'F'}];
          $scope.user = {};
          $scope.user.activated = true;
          $scope.user.notifyOnCancellation = true;
          $scope.user.notifyOnNewBooking = true;

          $scope.okUser = function () {
            console.log('okUser, saving record');
            if ($scope.user.password !== $scope.user.passwordRepeat) {
              $scope.alerts = [];
              $scope.alerts.push({type: 'danger', msg: 'Passwords do not match'});
              return;
            }
            $scope.user.passwordRepeat = null;
            if (!$scope.gender.name) {
              $scope.user.gender = 'M';
            } else {
              $scope.user.gender = $scope.gender.name.selected.name;
            }
            if (!$scope.user.administrator) {
              $scope.user.administrator = false;
              usergridFactory.createUser($scope.user)
                .then(function (user) {
                  console.log('user created');
                  usergridFactory.associateUserRole('default', user.uuid)
                    .then(function (data) {
                      usergridFactory.logEvent('normal', 'user', 'create', 'User created: ' + user.username);
                      console.log('associated');
                      $state.go('users.list');
                    },
                    function (errorMsg) {
                      console.log(errorMsg);
                      $scope.alerts = [];
                      $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user to role default'});
                    });
                },
                function (errorMsg) {
                  console.log('createUser failed');
                  $scope.alerts = [];
                  $scope.alerts.push({type: 'danger', msg: errorMsg});
                }
              )
              ;
            } else {
              // create admin user
              usergridFactory.createOrgAdmin(null, null, $scope.user)
                .then(function (user) {
                  console.log('user created');
                  usergridFactory.associateUserRole('admin', user.uuid)
                    .then(function (data) {
                      console.log('associated');
                      $state.go('users.list');
                    },
                    function (errorMsg) {
                      console.log(errorMsg);
                      $scope.alerts = [];
                      $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user to role default'});
                    });
                },
                function (errorMsg) {
                  console.log('createUser failed');
                  $scope.alerts = [];
                  $scope.alerts.push({type: 'danger', msg: errorMsg});
                }
              )
              ;
            }
          };

          $scope.cancel = function () {
            $state.go('users.list');
          };
        }]
      })
      //////////////////
      // Users > Edit //
      //////////////////
      .state('users.edit', {
        url: '/edit/:username',
        templateUrl: 'app/components/users/users.edit.html',
        controller: ['$scope', '$state', '$stateParams', '$filter', 'usergridFactory', function ($scope, $state, $stateParams, $filter, usergridFactory) {
          console.log('edit');
          $scope.$state = $state;
          var admin = undefined;
          usergridFactory.getUser($stateParams.username)
            .then(function (user) {
              admin = user[0].administrator;
              $scope.user = user[0];
              $scope.gender = {};
              $scope.genders = [{name: 'M'}, {name: 'F'}];
              $scope.gender.selected = {name: user[0].gender};
            });

          $scope.okUser = function () {
            if (!$scope.gender.name) {
              $scope.user.gender = 'M';
            } else {
              $scope.user.gender = $scope.gender.name.selected.name;
            }
            if (!$scope.user.administrator) {
              $scope.user.administrator = false;
            }
            usergridFactory.updateUser($scope.user)
              .then(function (user) {
                usergridFactory.logEvent('normal', 'user', 'modified', 'User updated: ' + user.username);
                if (admin !== user.administrator) {
                  var oldRole = admin ? 'admin' : 'default';
                  var newRole = user.administrator ? 'admin' : 'default';
                  usergridFactory.associateUserRole(newRole, user.uuid)
                    .then(function (data) {
                      usergridFactory.logEvent('normal', 'user', 'modified', 'User role updated: ' + user.username + ' now ' + newRole);
                      usergridFactory.removeUserRole(oldRole, user.uuid)
                        .then(function (data) {
                          usergridFactory.logEvent('normal', 'user', 'modified', 'User role updated: ' + user.username + ' removed ' + oldRole);
                          $state.go('users.list');
                        },
                        function (errorMsg) {
                          console.log(errorMsg);
                          $scope.alerts = [];
                          $scope.alerts.push({type: 'danger', msg: errorMsg + ' removing user role ' + oldRole});
                        });
                    },
                    function (errorMsg) {
                      console.log(errorMsg);
                      $scope.alerts = [];
                      $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user role ' + newRole});
                    });
                }
              },
              function (errorMsg) {
                console.log('updateUser failed');
                $scope.alerts = [];
                $scope.alerts.push({type: 'danger', msg: errorMsg});
              }
            )
            ;
          };

          $scope.cancel = function () {
            $state.go('users.list');
          };
        }]
      })
      ////////////////////
      // Users > Import //
      ////////////////////
      .state('users.import', {
        url: '/import',
        templateUrl: 'app/components/users/users.import.html',
        controller: ['$scope', '$state', '$filter', 'usergridFactory', 'ngTableParams', function ($scope, $state, $filter, usergridFactory, ngTableParams) {
          $scope.csv = {
            content: null,
            header: false,
            separator: ',',
            result: null
          };

          function createUser(user) {
            usergridFactory.createUser(user)
              .then(function (user) {
                usergridFactory.logEvent('normal', 'user', 'import', 'User import create: ' + username);
                console.log('user created');
                usergridFactory.associateUserRole('default', user.uuid)
                  .then(function (data) {
                    return true;
                    //$location.path('/userList');
                  },
                  function (errorMsg) {
                    usergridFactory.logEvent('error', 'user', 'import', 'User associateUserRole failure for: ' + username + ', ' + errorMsg);
                    $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user to role default'});
                    return false;
                  });
              },
              function (errorMsg) {
                usergridFactory.logEvent('error', 'user', 'import', 'User import create failure for: ' + username + ', ' + errorMsg);
                $scope.alerts.push({type: 'danger', msg: 'For user ' + user.username + ', ' + errorMsg});
                return false;
              });
          }

          // Import related code
          var reqdParmsTable = [
            {name: "username", desc: "Unique username", hasField: false},
            {name: "password", desc: "Users password", hasField: false},
            {name: "email", desc: "Unique email address", hasField: false}
          ];
          $scope.reqdParms = new ngTableParams({
            page: 1,
            count: 10
          }, {
            counts: [],
            total: reqdParmsTable.length,
            getData: function ($defer, params) {
              $defer.resolve(reqdParmsTable.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });

          var optlParmsTable = [
            {name: "membernum", desc: "Member Number"},
            {name: "firstname", desc: "First Name"},
            {name: "lastname", desc: "Last Name"},
            {name: "gender", desc: "M or F"},
            {name: "ghin", desc: "GHIN Number"},
            {name: "lockernum", desc: "Locker Number"},
            {name: "bagnum", desc: "Bag Location"},
            {name: "membertype", desc: "Membership Type. List of membership types specific to club"},
            {name: "<misc>", desc: "Make up your own field name and import"}
          ];
          $scope.optlParms = new ngTableParams({
            page: 1,
            count: 10
          }, {
            counts: [],
            total: optlParmsTable.length,
            getData: function ($defer, params) {
              $defer.resolve(optlParmsTable.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });

          $scope.$watch('csv.result', function () {
            if ($scope.csv.result) {
              $scope.importUserForm.header.$setValidity("requiredCsvParms", true);
              $scope.importUserForm.header.$setValidity("fileSelected", true);
              $scope.alerts = [];
              var headerFields = $scope.csv.result[0];
              // init required fields to false
              for (var reqdProp in reqdParmsTable) {
                reqdParmsTable[reqdProp].hasField = false;
              }
              // check for required fields
              for (var reqdProp in reqdParmsTable) {
                for (var prop in headerFields) {
                  if (headerFields[prop] === reqdParmsTable[reqdProp].name) {
                    reqdParmsTable[reqdProp].hasField = true;
                  }
                }
              }
              for (var reqdProp in reqdParmsTable) {
                if (!reqdParmsTable[reqdProp].hasField) {
                  $scope.alerts = [];
                  $scope.alerts.push({
                    type: 'danger',
                    msg: 'CSV file does not contain required field: ' + reqdParmsTable[reqdProp].name
                  });
                  $scope.importUserForm.header.$setValidity("requiredCsvParms", false);
                }
              }
            } else {
              if ($scope.importUserForm) {
                $scope.importUserForm.header.$setValidity("fileSelected", false);
              }
            }
          });

          $scope.okImport = function () {
            console.log('importing ' + $scope.csv.result.filename);
            var headerFields = $scope.csv.result[0];
            $scope.csv.result.forEach(function (user, i) {
              if (i > 0) {
                var userObj = {};
                for (var prop in headerFields) {
                  userObj[headerFields[prop]] = user[prop];
                }
                if (!userObj.name) {
                  userObj.name = userObj.firstname + ' ' + userObj.lastname;
                }
                userObj.activated = true;
                userObj.administrator = false;
                userObj.notifyOnCancellation = true;
                userObj.notifyOnNewBooking = true;
                // create user
                $scope.alerts = [];
                createUser(userObj);
              }
            })
          };
        }]
      })
    ;
  })
