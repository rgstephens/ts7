'use strict';
// factory that controls authentication, returns an object
// $cookies to create cookies
// $cookieStore to update or eliminate
// $location to load other routes
angular.module('ts7')
  .factory('auth', ['$cookies', '$cookieStore', '$location', '$rootScope', '$q', 'LocalStore', 'b64', function ($cookies, $cookieStore, $location, $rootScope, $q, LocalStore, b64) {
    return {
      loggedIn: function () {
        return (typeof($cookies.username) !== 'undefined');
      },
      getPassword: function () {
        return b64.decode($cookies.password);
      },
      checkStatus: function () {
        //Create an array of paths to be monitored
        var privateRoutes = ['/', '/user', '/login'];
        if (this.in_array($location.path(), privateRoutes) && typeof($cookies.username) === 'undefined') {
          // You're trying to access a private path and you aren't logged in
          $location.path('/login');
        }
        // If you try to access /login and you're already logged in, go /
        /*
         if (this.in_array("/login", privateRoutes) && typeof($cookies.username) != "undefined") {
         $location.path("/");
         }
         */
      },
      in_array: function (needle, haystack) {
        var key = '';
        for (key in haystack) {
          if (haystack[key] === needle) {
            return true;
          }
        }
        return false;
      }
    };
  }])
  .factory('LocalStore', ['$window', '$rootScope', 'cookieFactory', '$cookieStore', function ($window, $rootScope, cookieFactory, $cookieStore) {
    var cacheRoleList = null;
    var localFunctions = {
      set: function (key, value) {
        $window.localStorage.setItem(key, value);
      },
      // This sets the LocalStorage.user to the user value returned by the Usergrid standard login and appends data.user.authType
      setUser: function (value) {
        $window.localStorage.setItem('user', JSON.stringify(value));
      },
      // This sets the LocalStorage.user to the user value returned by the Usergrid admin login and appends data.user.authType
      storeCurrentServer: function (currentServer) {
        var settings = {currentServer: currentServer};
        $cookieStore.put('settings', settings);
      },
      setServerDefaults: function () {
        var currentServer = {
          description: 'default',
          // url: 'http://192.168.99.100:8080',
          url: 'http://tourneycard.teesheet.link:8080/ug',
          organization: 'BGC',
          application: 'TeeSheet',
          authType: 'user'
        };
        var settings = localFunctions.storeCurrentServer(currentServer);
        return settings.currentServer;
      },
      getCurrentServer: function () {
        var settings = $cookieStore.get('settings');
        if (typeof settings != 'undefined') {
          return settings.currentServer;
        } else {
          // not defined so set defaults
          settings = localFunctions.setServerDefaults();
          return settings.currentServer;
        }
      },
      getSettings: function () {
        var settings = $cookieStore.get('settings');
        if (typeof settings != 'undefined') {
          return settings;
        } else {
          return {};
        }
      },
      setRoles: function (value) {
        $window.localStorage['roles'] = value;
      },
      setAdminUser: function (value) {
        $window.localStorage.setItem('user', JSON.stringify(value));
        var roleList = []
        roleList.push('sysadmin');
        localFunctions.setRoles(roleList);
        cacheRoleList = roleList;
        $rootScope.$broadcast('rolesChanged');
        $rootScope.currentUser = value;
      },
      setPerson: function (value) {
        $window.localStorage.setItem('person', value);
        $rootScope.token = value.token;
      },
      getUser: function () {
        var dataJSON = $window.localStorage.getItem('user');
        if (dataJSON) {
          var data = JSON.parse(dataJSON);
          //$rootScope.token = data.token;
          return data
        } else {
          return dataJSON;
        }
      },
      getToken: function () {
        var dataJSON = $window.localStorage.getItem('user');
        //var dataJSON = localStorage.teesheetLogin;
        if (dataJSON) {
          var data = JSON.parse(dataJSON);
          return data.token
        } else {
          return null;
        }
      },
      deleteUser: function () {
        //console.log('LocalStore.deleteUser, deleting login related localStorage and cookies');
//        $rootScope.client.token = data.access_token;
        if (cookieFactory.getCookie('username')) {
          $cookieStore.remove('username');
          $cookieStore.remove('password');
        }
        if (cookieFactory.getCookie('token')) {
          $cookieStore.remove('token');
        }

        if (localStorage.user) {
          var data = JSON.parse(localStorage.user);
          $window.localStorage.removeItem('apigee_appName');
          $window.localStorage.removeItem('apigee_orgName');
          $window.localStorage.removeItem('apigee_token');
          $window.localStorage.removeItem('user');
          $window.localStorage.removeItem('person');
          $window.localStorage.removeItem('roles');
          $window.localStorage.removeItem('client');
          return data
        }
      },
      validateUser: function () {
        var user = this.getUser();
        if (user) {
          // fire off a valid user event
          $rootScope.$emit('teesheet:user-valid', user);
          return true
        } else {
          // fire off an invalid user event
          $rootScope.$emit('teesheet:user-invalid', user);
          return false
        }
      },
      removeItem: function(key) {
        $window.localStorage.removeItem(key);
      },
      setItem: function (key, value) {
        $window.localStorage[key] = value;
      },
      setObject: function (key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function (key) {
        if ($window.localStorage[key]) {
          return JSON.parse($window.localStorage[key]);
        } else {
          return null;
        }
        //return JSON.parse($window.localStorage[key] || '{}');
      },
      getString: function (key) {
        return $window.localStorage[key] || '{}';
      }
    }

    return localFunctions;
  }])
  .factory('cookieFactory', function () {
    return {
      getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') c = c.substring(1);
          if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
      }
    }
  })
  .factory('b64', function () {
    return {
      encode: function (str) {
        return window.btoa(decodeURI(encodeURIComponent(str)));
      },
      decode: function (str) {
        return decodeURIComponent(encodeURI(window.atob(str)));
      }
    }
  })
;
