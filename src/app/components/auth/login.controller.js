'use strict';

angular.module('ts7')
  .controller('LoginCtrl', function ($scope, $location, usergridFactory) {
    $scope.alerts = [];

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.login = function () {
      $scope.alerts = [];
      usergridFactory.login($scope.username, $scope.password)
        .then(function (user) { // success
          console.log('login, routing to home');
          $location.path("/");
          if(!$scope.$$phase) {
            $scope.$apply();
          }
        },
        function (errorMsg) { // error
          console.log('LoginCtrl: ' + errorMsg);
          $scope.alerts.push({type: 'danger', msg: errorMsg.message});
        }
      );
    }; // login function
  })
;
