'use strict';

angular.module('ts7')
  .controller('AppCtrl', function ($modal, $scope, $location, usergridFactory, $q, $filter, ngTableParams) {
//    $scope.apps = [{ name: 'MyApp'}, { name: 'YourApp'}];
    $scope.apps = [];
    $scope.users = [];
    $scope.orgSelected = '';
    function dynamicSort(property) {
      var sortOrder = 1;
      if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
      }
    }

    // List Orgs
    $scope.tableParams = new ngTableParams({
      page: 1,
      count: 10,
      sorting: {name: 'asc'}
    }, {
      getData: function ($defer, params) {
        //var user = LocalStore.getUser();
        var orgs = usergridFactory.listOrgs();
        var orgList = [];
        Object.keys(orgs).forEach(function (org) {
          var orgObj = {};
          orgObj.name = orgs[org].name;
          if (orgs[org].users) {
            Object.keys(orgs[org].users).forEach(function (user) {
              orgObj.username = orgs[org].users['admin'].username;
              orgObj.fullname = orgs[org].users['admin'].fullname;
              orgObj.email = orgs[org].users['admin'].email;
            });
          }
          orgList.push(orgObj);
        });
        params.total(orgList.length);
        //params.total(Object.keys(orgList).length);
        $defer.resolve(orgList);
      }
    });

    $scope.userTableParams = new ngTableParams({
      page: 1,
      count: 10,
      sorting: {name: 'asc'}
    }, {
      getData: function ($defer, params) {
        var appList = $scope.users;
/*
        var appList = [];
        usergridFactory.listApps($scope.orgSelected)
          .then(function (app) {
            for (var appName in app) {
              var appObj = {};
              appObj.name = appName;
              appObj.username = 'username';
              appObj.fullname = 'fullname';
              appObj.email = 'email';
              appList.push(appObj);
            }
          }),
          function (errorMsg) {
            console.log('usergridFactory.listApps error: ' + errorMsg);
          }
*/
        params.total(appList.length);
        $defer.resolve(appList);
      }
    });

    $scope.appTableParams = new ngTableParams({
      page: 1,
      count: 10,
      sorting: {name: 'asc'}
    }, {
      getData: function ($defer, params) {
        var appList = $scope.apps;
/*
        var appList = [];
        usergridFactory.listApps($scope.orgSelected)
          .then(function (app) {
            for (var appName in app) {
              var appObj = {};
              appObj.name = appName;
              appObj.username = 'username';
              appObj.fullname = 'fullname';
              appObj.email = 'email';
              appList.push(appObj);
            }
          });
*/
        params.total(appList.length);
        $defer.resolve(appList);
      }
    });

    $scope.viewOrg = function (orgname) {
      $scope.orgSelected = orgname;
      $location.path('/org/' + orgname);
    };

    $scope.org = {};
    $scope.org.selected = undefined;
    $scope.orgs = [];
    var orgList = usergridFactory.listOrgs();
    for (var key in orgList) {
      if (orgList.hasOwnProperty(key)) {
        var org = orgList[key];
        usergridFactory.getOrg(org.name)
          .then(function (fullOrg) {
            for (var u in fullOrg.users) {
              console.log(u);
              fullOrg.username = fullOrg.users[u].username;
              fullOrg.fullname = fullOrg.users[u].name;
              fullOrg.email = fullOrg.users[u].email;
            }
            $scope.orgs.push(fullOrg);
            $scope.orgs.sort(dynamicSort('name'));
          });
        $scope.appsOld = [];
        usergridFactory.listApps(org.name)
          .then(function (app) {
            for (var appName in app) {
              //console.log(appName + ' / ' + app[appName]);
              var appObj = {};
              //appObj[appName] = app[appName];
              appObj.name = appName;
              appObj.UUID = app[appName];
              appObj.orgName = org.name;
              appObj.orgUUID = org.uuid;
              $scope.apps.push(appObj);
            }
            //$scope.apps.sort(dynamicSort('name'));
          }),
          function (errorMsg) {
            console.log('usergridFactory.listApps error: ' + errorMsg);
          }
        usergridFactory.listAdminUsers(org.name)
          .then(function (users) {
            users.forEach(function (user) {
              $scope.users.push(user);
            });
          })

      }
    }


    $scope.deleteOrg = function (orgName, orgUUID) {
      //console.log('delete app clicked for app: ' + appName);
      $scope.appName = appName;
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        scope: $scope,
        templateUrl: 'app/components/apps/confirmModal.html',
        controller: 'ConfirmCtrl',
        resolve: {
          appName: function () {
            return $scope.appName;
          }
        }
      });
    };

    $scope.createApp = function () {
      $scope.alerts = [];
      console.log('createApp: ' + $scope.org.selected.name + '/' + $scope.name);
      if ($scope.adminPassword !== $scope.adminPasswordRepeat) {
        $scope.alerts.push({type: 'danger', msg: 'Passwords do not match'});
        return;
      }
      var appObj = {};
      appObj.name = $scope.name;
      appObj.registrationRequiresAdminApproval = true;
      /*  Create App  */
      usergridFactory.createApp($scope.org.selected.name, appObj)
        .then(function (data) {
          /*
           for (var i = 0; i < apps.length; i++)
           if (apps[i].UUID === app.UUID) {
           apps.splice(i, 1);
           break;
           }
           */
          /*  set Admin Role permissions  */
          usergridFactory.addPermissions($scope.org.selected.name, $scope.name, 'admin', 'roles', 'get,put,post,delete:/**')
            .then(function (data) {
              /*  create admin user  */
              var userObj = {};
              userObj.username = $scope.adminUsername;
              userObj.name = $scope.adminName;
              userObj.email = $scope.adminEmail;
              userObj.password = $scope.adminPassword;
              userObj.ghinNumber = '3315181';
              usergridFactory.createOrgAdmin($scope.org.selected.name, $scope.name, userObj)
                .then(function (user) {
                  /*  get admin role uuid  */
                  usergridFactory.getRole($scope.org.selected.name, $scope.name, 'admin')
                    .then(function (role) {
                      /*  associate user & role  */
                      usergridFactory.associateOrgAdminRole($scope.org.selected.name, $scope.name, role.uuid, user.uuid)
                        .then(function (data) {
                          console.log('worked');
                        },
                        function (errorMsg) {
                          console.log('associateOrgAdminRole failed');
                        });
                    },
                    function (errorMsg) {
                      console.log('getrole failed');
                    });
                },
                function (errorMsg) {
                  console.log('createOrgAdmin failed');
                });
            },
            function (errorMsg) {
              console.log('permissions failed');
            });
          $location.path("/appList");
        },
        function (errorMsg) {
          console.log('apps.js: app create failed: ' + errorMsg);
          $scope.alerts.push({type: 'danger', msg: errorMsg});
        });
    };

    $scope.deleteApp = function (apps, app) {
      //console.log('delete app clicked for app: ' + appName);
      $scope.apps = apps;
      $scope.app = app;
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        scope: $scope,
        templateUrl: 'app/components/apps/confirmModal.html',
        controller: 'ConfirmCtrl',
        resolve: {
          appName: function () {
            return $scope.appName;
          }
        }
      });
    };
  })
  .controller('ConfirmCtrl', function ($scope, $modalInstance, usergridFactory) {

    $scope.ok = function (app, apps) {
      console.log('calling delete app for app: ' + app.name);
      usergridFactory.deleteApp(app.orgName, app.name)
        .then(function (data) {
          console.log('app deleted: ' + app.name);
          for (var i = 0; i < apps.length; i++)
            if (apps[i].UUID === app.UUID) {
              apps.splice(i, 1);
              break;
            }
        },
        function (errorMsg) {
          console.log('app delete failed for app: ' + errorMsg);
        });
      $modalInstance.close();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })
;

