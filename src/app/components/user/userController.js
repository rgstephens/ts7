'use strict';

angular.module('ts7')
  .config(function ($stateProvider) {
    $stateProvider
      .state('user', {
        url: '/user',
        templateUrl: 'app/components/user/user.html',
        controller: 'UserCtrl'
      })
      .state('userList', {
        url: '/userList',
        templateUrl: 'app/components/user/listUsers.html',
        controller: 'UserCtrl'
      })
      .state('userCreate', {
        url: '/userCreate',
        templateUrl: 'app/components/user/createUser.html',
        controller: 'UserCtrl'
      })
      .state('userImport', {
        url: '/userImport',
        templateUrl: 'app/components/user/importUsers.html',
        controller: 'UserCtrl'
      })
      .state('userEdit', {
        url: '/user/:username',
        templateUrl: 'app/components/user/createUser.html',
        controller: 'UserCtrl',
/*
        controller: function($scope, userState) {
          $scope.user = userState;
        },
*/
        resolve: {
          userState: ['$stateParams', 'usergridFactory',
            function($stateParams, usergridFactory) {
              return usergridFactory.getUser($stateParams.username)
                .then(function (user) {
                  return user[0];
                });
            }]
        }
      })
;
  })
    .controller('UserCtrl', function ($scope, $state, $location, $q, $filter, $parse, ngTableParams, usergridFactory) {
    $scope.alerts = [];
    $scope.gender = {};
    $scope.gender.selected = {name: 'M'};
    $scope.genders = [{name: 'M'}, {name: 'F'}];
/*
    if (userState) {
      $scope.user = userState;
    } else {
    }
*/
    $scope.user = {};
    $scope.user.active = true;
    $scope.user.notifyOnCancellation = true;
    $scope.user.notifyOnNewBooking = true;
    $scope.csv = {
      content: null,
      header: false,
      separator: ',',
      result: null
//      separatorVisible: '!='
    };

    function toPrettyJSON(json, tabWidth) {
      var objStr = JSON.stringify(json);
      var obj = null;
      try {
        obj = $parse(objStr)({});
      } catch (e) {
        // eat $parse error
        return _lastGoodResult;
      }

      var result = JSON.stringify(obj, null, Number(tabWidth));

      return result;
    }

    function createUser(user) {
      usergridFactory.createUser(user)
        .then(function (user) {
          console.log('user created');
          usergridFactory.associateUserRole('default', user.uuid)
            .then(function (data) {
              console.log('associated');
              return true;
              //$location.path('/userList');
            },
            function (errorMsg) {
              console.log(errorMsg);
              $scope.alerts = [];
              $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user to role default'});
              return false;
            });
        },
        function (errorMsg) {
          console.log('createUser failed');
          $scope.alerts = [];
          $scope.alerts.push({type: 'danger', msg: errorMsg});
          return false;
        });
    }

    $scope.tableParams = new ngTableParams({
      page: 1,
      count: 10,
      sorting: {username: 'asc'}
    }, {
      getData: function ($defer, params) {
        //var users = [];
        usergridFactory.listUsers()
          .then(function (users) {
            //$defer.resolve(users);
            params.total(users.length);
            // append the user Role to each user
            users.forEach(function (user, i) {
              usergridFactory.getUserRoles(user.username)
                .then(function (roleList) {
                  user.roles = roleList;
                  if (i >= (users.length - 1)) {
                    users = params.filter() ? $filter('filter')(users, params.filter()) : users;
                    users = params.sorting() ? $filter('orderBy')(users, params.orderBy()) : users;
                    $defer.resolve(users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                  }
                })
            });
          });
      }
    });

    // Import related code
    var reqdParmsTable = [
      {name: "username", desc: "Unique username", hasField: false},
      {name: "password", desc: "Users password", hasField: false},
      {name: "email", desc: "Unique email address", hasField: false}
    ];
    $scope.reqdParms = new ngTableParams({
      page: 1,
      count: 10
    }, {
      counts: [],
      total: reqdParmsTable.length,
      getData: function ($defer, params) {
        $defer.resolve(reqdParmsTable.slice((params.page() - 1) * params.count(), params.page() * params.count()));
      }
    });

    var optlParmsTable = [
      {name: "membernum", desc: "Member Number"},
      {name: "firstname", desc: "First Name"},
      {name: "lastname", desc: "Last Name"},
      {name: "gender", desc: "M or F"},
      {name: "ghin", desc: "GHIN Number"},
      {name: "lockernum", desc: "Locker Number"},
      {name: "bagnum", desc: "Bag Location"},
      {name: "membertype", desc: "Membership Type. List of membership types specific to club"},
      {name: "<misc>", desc: "Make up your own field name and import"}
    ];
    $scope.optlParms = new ngTableParams({
      page: 1,
      count: 10
    }, {
      counts: [],
      total: optlParmsTable.length,
      getData: function ($defer, params) {
        $defer.resolve(optlParmsTable.slice((params.page() - 1) * params.count(), params.page() * params.count()));
      }
    });

    $scope.$watch('csv.result', function () {
      if ($scope.csv.result) {
        $scope.importUserForm.header.$setValidity("requiredCsvParms", true);
        $scope.importUserForm.header.$setValidity("fileSelected", true);
        $scope.alerts = [];
        var headerFields = $scope.csv.result[0];
        // init required fields to false
        for (var reqdProp in reqdParmsTable) {
          reqdParmsTable[reqdProp].hasField = false;
        }
        // check for required fields
        for (var reqdProp in reqdParmsTable) {
          for (var prop in headerFields) {
            if (headerFields[prop] === reqdParmsTable[reqdProp].name) {
              reqdParmsTable[reqdProp].hasField = true;
            }
          }
        }
        for (var reqdProp in reqdParmsTable) {
          if (!reqdParmsTable[reqdProp].hasField) {
            $scope.alerts = [];
            $scope.alerts.push({
              type: 'danger',
              msg: 'CSV file does not contain required field: ' + reqdParmsTable[reqdProp].name
            });
            $scope.importUserForm.header.$setValidity("requiredCsvParms", false);
          }
        }
      } else {
        if ($scope.importUserForm) {
          $scope.importUserForm.header.$setValidity("fileSelected", false);
        }
      }
    });

    $scope.okUser = function () {
      console.log('okUser, processing csv.result');
      if ($scope.user.password !== $scope.user.passwordRepeat) {
        $scope.alerts = [];
        $scope.alerts.push({type: 'danger', msg: 'Passwords do not match'});
        return;
      }
      $scope.user.passwordRepeat = null;
      if (!$scope.gender.name) {
        $scope.user.gender = 'M';
      } else {
        $scope.user.gender = $scope.gender.name.selected.name;
      }
      if (!$scope.user.administrator) {
        usergridFactory.createUser($scope.user)
          .then(function (user) {
            console.log('user created');
            usergridFactory.associateUserRole('default', user.uuid)
              .then(function (data) {
                console.log('associated');
                $location.path('/userList');
              },
              function (errorMsg) {
                console.log(errorMsg);
                $scope.alerts = [];
                $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user to role default'});
              });
          },
          function (errorMsg) {
            console.log('createUser failed');
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: errorMsg});
          }
        )
        ;
      } else {
        // create admin user
        usergridFactory.createOrgAdmin(null, null, $scope.user)
          .then(function (user) {
            console.log('user created');
            usergridFactory.associateUserRole('admin', user.uuid)
              .then(function (data) {
                console.log('associated');
                $location.path('/userList');
              },
              function (errorMsg) {
                console.log(errorMsg);
                $scope.alerts = [];
                $scope.alerts.push({type: 'danger', msg: errorMsg + ' assigning user to role default'});
              });
          },
          function (errorMsg) {
            console.log('createUser failed');
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: errorMsg});
          }
        )
        ;
      }
    };

    $scope.okImport = function () {
      console.log('importing ' + $scope.csv.result.filename);
      var headerFields = $scope.csv.result[0];
      $scope.csv.result.forEach(function (user, i) {
        if (i > 0) {
          var userObj = {};
          for (var prop in headerFields) {
            userObj[headerFields[prop]] = user[prop];
          }
          if (!userObj.name) {
            userObj.name = userObj.firstname + ' ' + userObj.lastname;
          }
          // create user
          createUser(userObj);
        }
      })
    };

    /*

     $scope.editUser = function (username) {
     console.log('edit user: ' + username);
     };
     */

    $scope.deleteUser = function (username) {
      console.log('delete user: ' + username + ', reloading table');
      usergridFactory.deleteUser(username)
        .then(function (user) {
          for (var i = $scope.tableParams.data.length - 1; i >= 0; i--) {
            if ($scope.tableParams.data[i].username == username) {
              $scope.tableParams.data.splice(i, 1);
            }
          }
          $scope.tableParams.reload();
        });
      /*
       for (var i = $scope.users.length - 1; i >= 0; i--) {
       if ($scope.users[i].username == username) {
       $scope.users.splice(i, 1);
       }
       }
       */
    };

    $scope.cancel = function () {
      $location.path('/userList');
    };
  })
;
