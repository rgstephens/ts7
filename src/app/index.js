'use strict';

angular.module('ts7', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'ui.select', 'ngTable', 'ngCsvImport'])
  .config(function ($stateProvider, $urlRouterProvider, usergridFactoryProvider) {

    //var isAuthenticatedLocal = usergridFactory.isAuthenticated();

    // http://www.frederiknakstad.com/2014/02/09/ui-router-in-angular-client-side-auth/
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        data: {
          access: usergridFactoryProvider.$get().isAuthenticated()
        }
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/components/auth/login.html',
        controller: 'LoginCtrl'
      })
      .state('logout', {
        url: '/logout',
        templateUrl: 'app/components/auth/login.html',
        controller: function (usergridFactory) {
          usergridFactory.logout();
        }
      })
      .state('orgList', {
        url: '/orgList',
        templateUrl: 'app/components/apps/listOrgs.html',
        controller: 'AppCtrl'
      })
      .state('org', {
        url: '/org/:name',
        templateUrl: 'app/components/apps/listApps.html',
        controller: 'AppCtrl'
      })
      .state('appList', {
        url: '/appList',
        templateUrl: 'app/components/apps/listApps.html',
        controller: 'AppCtrl'
      })
      .state('appCreate', {
        url: '/appCreate',
        templateUrl: 'app/components/apps/createApp.html',
        controller: 'AppCtrl'
      })
    ;

    // http://stackoverflow.com/questions/17485900/injecting-dependencies-in-config-modules-angularjs
    if (usergridFactoryProvider.$get().isAuthenticated()) {
      console.log('DEFAULT ROUTE SET TO HOME');
      $urlRouterProvider.otherwise('/');
    } else {
      console.log('DEFAULT ROUTE SET TO /Login');
      $urlRouterProvider.otherwise('/login');
    }
  })

  // .run is executed after .config
  // Perform run-time auth checks to insure user has access to the route
  .run(function ($rootScope, auth, LocalStore, usergridFactory) {
//    var isAuthenticated = usergridFactory.isAuthenticated();

    // if there's no default currentServer set one
    var currentServer = LocalStore.getCurrentServer();
    if (!currentServer) {
      currentServer = {
        description: 'default',
        url: 'http://tourneycard.teesheet.link:8080/ug',
        organization: 'myfirstorg',
        application: 'myapp'
      };
      LocalStore.storeCurrentServer(currentServer);
    }

    // When the route changes
    $rootScope.$on('$stateChangeStart', function () {
      // check users status from the login.factory
      auth.checkStatus();
    });
  })
;
