'use strict';

//var ugModule = angular.module('ugModule', []);

angular.module('ts7')
  .factory('usergridFactory', function ($cookieStore, $cookies, $location, $q, LocalStore, b64) {

    var localFunctions = {
      ///////////////////////////////
      // Login/Logout/Credentials  //
      ///////////////////////////////
      login: function (username, password) {
        // validate user with server....
        // if valid, save user, org, authLevel as a cookie
        // create the cookies
        // Get the settings values in case they've changed
        settings = LocalStore.getSettings();
        //LocalStore.deleteUser();  // delete old user if they're hanging around in local storage
        var options = {};
        // access token calls: http://apigee.com/docs/app-services/content/access-token
        switch (settings.currentServer.authType) {
          case 'admin':
            // POST "https://api.usergrid.com/management/token" -d '{"grant_type":"password", "username":<admin_username>, "password":<admin_password>}'
            options = {
              method: 'POST',
              endpoint: 'management/token',
              body: {
                username: username,
                password: password,
                grant_type: "password"
              },
              mQuery: true //specifies that we are not targeting an orgName/appName endpoint
            };
            break;
          case 'user':
            // POST "https://api.usergrid.com/<orgName>/<appName>/token" -d '{"grant_type":"password", "username":<username>, "password":<password>}'
            client.orgName = settings.currentServer.organization;
            client.appName = settings.currentServer.application;
            /*
             client = new Usergrid.Client({
             URI: settings.currentServer.url,
             orgName: settings.currentServer.organization,
             appName: settings.currentServer.application,
             logging: false,
             buildCurl: true
             });
             */
            options = {
              method: 'POST',
              endpoint: 'token',
              body: {
                username: username,
                password: password,
                grant_type: "password"
              },
              mQuery: false
            };
            break;
          case 'org':
            // POST "https://api.usergrid.com/management/token" -d '{"grant_type":"client_credentials", "client_id":<org_clientID>, "client_secret":<org_client_secret>}'
            options = {
              method: 'POST',
              endpoint: 'token',
              body: {
                client_id: username,
                client_secret: password,
                grant_type: "client_credentials"
              },
              mQuery: false
            };
            break;
          case 'app':
            // POST "https://api.usergrid.com/<orgName>/<appName>/token" -d '{"grant_type":"client_credentials", "client_id":<application_clientID>, "client_secret":"<application_client_secret>"}'
            options = {
              method: 'POST',
              endpoint: settings.currentServer.organization + '/' + settings.currentServer.application + '/token',
              body: {
                client_id: username,
                client_secret: password,
                grant_type: "client_credentials"
              },
              mQuery: false
            };
            break;
          default:
            console.log('authType: defaults');
        }
        var deferred = $q.defer();
        // Make the Login call. client attributes: URI, buildCurl, logging, token=null
        client.token = null;
        client.request(options, function (error, data) {
          var user = {};
          if (error) {
            console.log('usergridFactory.js: login attempt for user: ' + username + ', error: ' + error);
            deferred.reject(error);
          } else {
            // login succeeded
            $cookies.username = b64.encode(username);
            $cookies.password = b64.encode(password);
            client.token = data.access_token;
            LocalStore.setObject('client', client);
            switch (settings.currentServer.authType) {
              case 'admin':
                $cookies.token = data.access_token;
                data.user.authType = settings.currentServer.authType;
                LocalStore.setAdminUser(data.user);
                console.log('usergridFactory.js: admin login succeeded for: ' + username + ', token: ' + client.token);
                deferred.resolve(data.user);
                break;
              case 'user':
                //$cookies.token = client.token;
                console.log('user login succeeded for: ' + username + ', token: ' + client.token);
                client.getLoggedInUser(function (err, data, user) {
                  console.log('getLoggedInUser, err: ' + err);
                  if (err) {
                    console.log('usergridFactory.js: could not get user data: ' + err);
                    deferred.reject('could not get user data: ' + err);
                  } else {
                    user.authType = settings.currentServer.authType;
                    console.log('usergridFactory.js: user data: ' + user._data);
                    LocalStore.setUser(user._data);
                    localFunctions.getUserRoles(username)
                      .then(function (roleList) {
                        LocalStore.setRoles(roleList);
                        deferred.resolve(user._data);
                      });
                  }
                });
                break;
            }
          }
        });
        return deferred.promise;
      },
      logoutCallback: function (error) {
        console.log('logoutCallback');
        LocalStore.deleteUser();
      },
      logout: function () {
        //Logout when you remove the cookie
        $cookieStore.remove('username');
        $cookieStore.remove('password');
        $cookieStore.remove('token');
        var currentUser = LocalStore.getUser();
        LocalStore.deleteUser();
        //var client = LocalStore.getObject('client');
        if (client) {
          //client.logoutAndDestroyToken(currentUser.username, currentUser.token, true, localFunctions.logoutCallback());
          //client.logoutAndDestroyToken(currentUser.username, currentUser.token, true);
/*
          client.logoutAndDestroyToken(currentUser.username, currentUser.token, true, function(err) {
            if (err) {
              console.log('logout error: ' + err);
            } else {
              console.log('successful logout');
            }
          });
*/
          var currentServer = LocalStore.getCurrentServer();
          switch (settings.currentServer.authType) {
            case 'admin':
              // PUT https://api.usergrid.com/<org_name>/<app_name>/users/<user_uuid_or_username>/revoketokens
              var options = {
                method: 'PUT',
                endpoint: 'management/users/' + currentUser.username + '/revoketokens',
                mQuery: true
              };
              break;
            case 'user':
              // PUT https://api.usergrid.com/<org_name>/<app_name>/users/<user_uuid_or_username>/revoketokens
              var options = {
                method: 'PUT',
                endpoint: 'users/' + currentUser.username + '/revoketokens'
                //endpoint: currentServer.organization + '/' + currentServer.application + '/users/' + currentUser.username + '/revoketoken'
              };
              break;
          }
          var deferred = $q.defer();
          client.request(options, function (err, data) {
            if (err) {
              console.log('usergridFactory.js: logout failure: ' + currentUser.username + ", error: " + err.message);
              deferred.reject(err.message);
              //return false;
            } else {
              deferred.resolve(data);
            }
          });
          return deferred.promise;
        }
      },
      getAppCreds: function () {
        //var clientSettings = LocalStore.getObject('client');
        var options = {
          method: 'GET',
          endpoint: 'management/orgs/' + client.orgName + '/apps/' + client.appName + '/credentials',
          mQuery: true
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure getting app credentials: ' + client.orgName + ", error: " + err.message);
            deferred.reject('failure getting apps for organization: ' + client.orgName + ', error: ' + err.message);
            //return false;
          } else {
            deferred.resolve(data.credentials);
          }
        });
        return deferred.promise;
      },
      isAuthenticated: function () {
        //var authenticated = client.get('token') !== null && client.get('orgName') !== null && typeof($cookies.username) !== 'undefined';
        var authenticated = client.get('token') !== null && typeof($cookies.username) !== 'undefined';
        return authenticated;
      },
      logEvent: function (severity, category, activity, message) {
        var options = {
          method: 'POST',
          endpoint: 'events'
        };
        var user = LocalStore.getObject('user');
        options.body = {};
        options.body.timestamp = 0;
        options.body.category = category;
        options.body.activity = activity;
        options.body.user = user.uuid;
        options.body.username = user.username;
        //options.body.group = 'abc';
        options.body.message = message;
        options.body.severity = severity;
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure creating user: ' + userObj.name + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data.entities[0]);
          }
        });
        return deferred.promise;
      },
      //////////////////////
      // Orgs/Apps?users  //
      //////////////////////
      listOrgs: function () {
        var user = LocalStore.getUser();
        return user.organizations;
      },
      getOrg: function (org) {
        var options = {
          method: 'GET',
          endpoint: 'management/orgs/' + org,
          mQuery: true
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure getting organization: ' + org + ", error: " + err.message);
            deferred.reject('failure getting organization: ' + org + ', error: ' + err.message);
            //return false;
          } else {
            deferred.resolve(data.organization);
          }
        });
        return deferred.promise;
      },
      listApps: function (org) {
        var options = {
          method: 'GET',
          endpoint: 'management/organizations/' + org + '/applications',
          mQuery: true //specifies that we are not targeting an orgName/appName endpoint
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure getting apps for organization: ' + org + ", error: " + err.message);
            deferred.reject('failure getting apps for organization: ' + org + ', error: ' + err.message);
            //return false;
          } else {
            deferred.resolve(data.data);
          }
        });
        return deferred.promise;
      },
      listAdminUsers: function (org) {
        var options = {
          method: 'GET',
          endpoint: 'management/organizations/' + org + '/users',
          mQuery: true //specifies that we are not targeting an orgName/appName endpoint
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure getting users for organization: ' + org + ", error: " + err.message);
            deferred.reject('failure getting users for organization: ' + org + ', error: ' + err.message);
            //return false;
          } else {
            deferred.resolve(data.data);
          }
        });
        return deferred.promise;
      },
      createApp: function (orgName, appObj) {
        var options = {
          method: 'POST',
          endpoint: 'management/orgs/' + orgName + '/apps',
          mQuery: true
        };
        options.body = appObj;
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure creating app: ' + appName + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data.data);
          }
        });
        return deferred.promise;
      },
      createOrgAdmin: function (orgName, appName, userObj) {
        if (!orgName) {
          orgName = client.orgName;
        }
        if (!appName) {
          appName = client.appName;
        }
        var options = {
          method: 'POST',
          endpoint: orgName + '/' + appName + '/users',
          mQuery: true
        };
        options.body = userObj;
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure creating orgAdmin: ' + userObj.name + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data.entities[0]);
          }
        });
        return deferred.promise;
      },
      createUser: function (userObj) {
        var options = {
          method: 'POST',
          endpoint: 'users'
        };
        options.body = userObj;
        var saveToken = client.token;
        client.token = null;
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          client.token = saveToken;
          if (err) {
            console.log('usergridFactory.js: failure creating user: ' + userObj.name + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data.entities[0]);
          }
        });
        return deferred.promise;
      },
      updateUser: function (userObj) {
        var options = {
          method: 'PUT',
          endpoint: 'users/' + userObj.username
        };
        options.body = userObj;
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure creating user: ' + userObj.name + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data.entities[0]);
          }
        });
        return deferred.promise;
      },
      setUserPassword: function (username, oldpassword, newpassword) {
        var options = {
          method: 'PUT',
          endpoint: 'users/' + username + '/password'
        };
        options.body = {};
        options.body.oldpassword = oldpassword;
        options.body.newpassword = newpassword;
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure changing password for user: ' + username + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data);
          }
        });
        return deferred.promise;
      },
      resetPassword: function (username) {
        var options = {
          method: 'GET',
          endpoint: 'users/' + username + '/resetpw'
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure resetting password for user: ' + username + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data.entities[0]);
          }
        });
        return deferred.promise;
      },
      deleteUser: function (username) {
        var options = {
          method: 'DELETE',
          endpoint: 'users/' + username
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure deleting user: ' + username + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data);
          }
        });
        return deferred.promise;
      },
      associateOrgAdminRole: function (orgName, appName, roleUUID, userUUID) {
        var options = {
          method: 'POST',
          endpoint: orgName + '/' + appName + '/roles/' + roleUUID + '/users/' + userUUID,
          mQuery: true
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          console.log('test1');
          if (err) {
            console.log('usergridFactory.js: failure making user role association: ' + err.message);
            deferred.reject(err.message);
          } else {
            deferred.resolve(data);
          }
        });
        console.log('test2');
        return deferred.promise;
      },
      associateUserRole: function (rolename, userUUID) {
        var options = {
          method: 'POST',
          endpoint: 'roles/' + rolename + '/users/' + userUUID,
          mQuery: false
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          console.log('test1');
          if (err) {
            console.log('usergridFactory.js: failure making user role association: ' + err.message);
            deferred.reject(err.message);
          } else {
            deferred.resolve(data);
          }
        });
        console.log('test2');
        return deferred.promise;
      },
      removeUserRole: function (rolename, userUUID) {
        var options = {
          method: 'DELETE',
          endpoint: 'roles/' + rolename + '/users/' + userUUID,
          mQuery: false
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          console.log('test1');
          if (err) {
            console.log('usergridFactory.js: failure making user role association: ' + err.message);
            deferred.reject(err.message);
          } else {
            deferred.resolve(data);
          }
        });
        console.log('test2');
        return deferred.promise;
      },
      listUsers: function () {
/*
        var currentServer = LocalStore.getCurrentServer();
        switch (settings.currentServer.authType) {
          case 'admin':
            // admin: http://192.168.99.100:8080/orgs/nworks/apps/b/users - only lists admin users
            var options = {
              method: 'PUT',
              endpoint: 'management/orgs/' + currentServer.organization + '/apps/' + currentServer.application + '/users',
              mQuery: true
            };
            break;
          case 'user':
            // user:  http://192.168.99.100:8080/nworks/b/users?limit=50&access_token=YWM....
            var options = {
              method: 'GET',
              endpoint: 'users',
              qs: {limit: 50},
              mQuery: false
            };
            break;
        }
*/
        // user:  http://192.168.99.100:8080/nworks/b/users?limit=50&access_token=YWM....
        var options = {
          method: 'GET',
          endpoint: 'users',
          qs: {limit: 50},
          mQuery: false
        };
        if (settings.currentServer.authType === 'admin') {
          client.orgName = settings.currentServer.organization;
          client.appName = settings.currentServer.application;
        }
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('failure getting user list: ' + err);
            deferred.reject('failure getting user list: ' + err);
          } else {
            deferred.resolve(data.entities);
          }
        });
        return deferred.promise;
      },
      getUser: function (userName) {
        var options = {
          method: 'GET',
          endpoint: 'users/' + userName
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('failure getting user ' + userName + ': ' + err);
            deferred.reject('failure getting user ' + userName + ': ' + err);
          } else {
            deferred.resolve(data.entities);
          }
        });
        return deferred.promise;
      },
      getUserRoles: function (userName) {
        var roleList = [];
        var roleStr = '';
        var options = {
          method: 'GET',
          endpoint: 'users/' + userName + '/roles'
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('failure getting roles for user ' + userName + ': ' + err);
            deferred.reject('failure getting roles for user ' + userName + ': ' + err);
          } else {
            //$rootScope.currentUser = value;
            data.entities.forEach(function (it) {
              roleList.push(it.name);
              if (roleStr) {
                roleStr = ',' + it.name;
              } else {
                roleStr = it.name;
              }
            });
            deferred.resolve(roleStr);
            //cacheRoleList = roleList;
            //$window.localStorage.setItem('roles', roleList);
            //LocalStore.setObject('roles', roleList);
            //$rootScope.$broadcast('rolesChanged');
            //$rootScope.token = value.token;
            //JSON.stringify(user._data)
          }
        });
        return deferred.promise;
      },
      getRole: function (orgName, appName, roleName) {
        var options = {
          method: 'GET',
          endpoint: orgName + '/' + appName + '/roles/' + roleName,
          mQuery: true
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure getting role: ' + err.message);
            deferred.reject(err.message);
          } else {
            deferred.resolve(data.entities[0]);
          }
        });
        return deferred.promise;
      },
      addPermissions: function (org, app, entityName, entityType, permissions) {
        var options = {
          method: 'POST',
          endpoint: org + '/' + app + '/' + entityType + '/' + entityName + '/permissions',
          mQuery: true
        };
        options.body = {permission: permissions};
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure adding permissions: ' + entityName + ", error: " + err.message);
            deferred.reject(err.message);
            //return false;
          } else {
            deferred.resolve(data);
          }
        });
        return deferred.promise;
      },
      deleteApp: function (orgName, appName) {
        // DELETE /management/organizations|orgs/{org_name}|{org_uuid}/apps/{app_name}|{app_uuid}
        // DELETE /management/orgs/{org_name}/apps/{app_name}
        console.log('usergridFactory.deleteApp');
        var options = {
          method: 'DELETE',
          endpoint: 'management/orgs/' + orgName + '/apps/' + appName.split("/")[1],
          mQuery: true
        };
        var deferred = $q.defer();
        client.request(options, function (err, data) {
          if (err) {
            console.log('usergridFactory.js: failure deleting app: ' + appName + ", error: " + err.message);
            deferred.reject('failure deleting app: ' + appName + ", error: " + err.message);
            //return false;
          } else {
            deferred.resolve(data);
          }
        });
        return deferred.promise;
      }
    }

    // This block runs on load or reload
    var settings = LocalStore.getSettings();
    var currentServer = LocalStore.getCurrentServer();
    // check if the client info is stored in Local storage, if not, then set it.
    /*
     var client = new Usergrid.Client({
     URI: settings.currentServer.url,
     logging: false,
     buildCurl: false
     });
     */
    var client;
    var user = LocalStore.getUser();
    if (!user) {
      client = new Usergrid.Client({
        URI: currentServer.url
/*
        logging: false,
        buildCurl: false
*/
      });
    } else {
      // do we have to re-do the login to get the client object setup correctly?  Let's try:
      client = new Usergrid.Client({
        URI: currentServer.url
/*
        logging: false,
        buildCurl: false
*/
      });
      var oldClient = LocalStore.getObject('client');
      var roles = LocalStore.getString('roles');
      client.token = oldClient.token;
      //client = Usergrid.Client.prototype.reAuthenticate(user.email, function(error, data) {
      // This appears to be a management user reauthenticate only - https://github.com/apache/incubator-usergrid/blob/49ae4ac5b8d5d77e90e6e6c6e9d8b299a5423863/sdks/html5-javascript/usergrid.js

      if (roles === 'sysadmin') {
        console.log('usergridFactory.main, sysadmin login');
        client.reAuthenticateLite(function (error, response) {
          console.log("reAuthenticate return: " + error);
          if (error) {
            console.log('reAuthenticate error: ' + error);
            LocalStore.deleteUser();
          } else {
            console.log('reAuthenticate successful, going to home page');
            $location.path('/home');
          }
        });
      } else {
        console.log('usergridFactory.main, regular user login');
        if ($cookies.username) {
          var username = b64.decode($cookies.username);
          var password = b64.decode($cookies.password);
          localFunctions.login(username, password);
          $location.path('/home');
        } else {
          $location.path('/login');
        }
      }
      //var user = LocalStore.getUser();
      //logout();
      //login(user.username, auth.getPassword());
    }
    return localFunctions;
  })
;
